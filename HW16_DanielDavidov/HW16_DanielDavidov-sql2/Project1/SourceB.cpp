#include "sqlite3.h"
#include <iostream>
#include <sstream>
using namespace std;

int callback(void* idc, int argc, char* argv[], char* idcagain[]);
int printTable(void* idc, int argc, char* argv[], char* idcagain[]);
int printSelect(void* idc, int argc, char* argv[], char* idcagain[]);
void whoCanBuy(int carId, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
void checkErr(int rc, char* zErrMsg);
void checkOpen(sqlite3* db, int rc);

int main()
{
	sqlite3* db;
	int rc;
	char* zErrMsg = 0;
	rc = sqlite3_open("carsDealer.db", &db);
	checkOpen(db, rc);
	
	rc = sqlite3_exec(db, "select * from accounts;", printTable, 0, &zErrMsg);
	checkErr(rc, zErrMsg);

	bool result = carPurchase(12, 17, db, zErrMsg);
	if (result) std::cout << "The purchase succeed!" << endl;
	else std::cout << "unsuccessful purchase" << endl;
	
	result = carPurchase(1,2,db, zErrMsg);
	if (result) std::cout << "The purchase succeed!" << endl;
	else std::cout << "unsuccessful purchase" << endl;

	result = carPurchase(12, 1, db, zErrMsg);
	if (result) std::cout << "The purchase succeed!" << endl;
	else std::cout << "unsuccessful purchase" << endl;
	
	result = balanceTransfer(1, 2, 20000, db, zErrMsg);
	if (result) std::cout << "The purchase succeed!" << endl;
	else std::cout << "unsuccessful purchase" << endl;
	
	rc = sqlite3_exec(db, "select * from accounts;", printTable, 0, &zErrMsg); checkErr(rc, zErrMsg);
	checkErr(rc, zErrMsg);

	cout << endl;
	whoCanBuy(3, db, zErrMsg);
	
	sqlite3_close(db);
	system("PAUSE");
	return 0;
}


int callback(void* idc, int argc, char* argv[], char* idcagain[])
{
	*((int*)idc) = (int)(stoi(string(argv[0]))); //cout << argv[0] << endl;
	return 0;
}

int printTable(void* idc, int argc, char* argv[], char* idcagain[])
{
	for (int i = 0; i < argc; i += 3)
	{
		std::cout << argv[i] << " | " << argv[i + 1] << " | " << argv[i + 2] << endl;
	}
	return 0;
}

int printSelect(void* idc, int argc, char* argv[], char* idcagain[])
{
	cout << argv[0] << "|" << argv[1] << endl;
	return 0;
}

void whoCanBuy(int carId, sqlite3* db, char* zErrMsg)
{
	int rc;
	int* accounts = new int;
	
	stringstream ss;
	ss << carId;
	string carsStr = ss.str();

	string getCars = "select count(*), color from cars where id=" + carsStr + ";"; // only gets number of cars and they're color
	string getCountAccounts = "select count(*) from accounts;"; // gets number of accounts;
	rc = sqlite3_exec(db, getCars.c_str(), printSelect, 0, &zErrMsg);
	checkErr(rc, zErrMsg);
	rc = sqlite3_exec(db, getCountAccounts.c_str(), callback, accounts, &zErrMsg);
	checkErr(rc, zErrMsg);
	for (int i = 0; i < *accounts; i++)
	{
		rc = sqlite3_exec(db, "begin transaction;", NULL, 0, &zErrMsg);
		checkErr(rc, zErrMsg);
		bool canBuy = carPurchase(i, carId, db, zErrMsg);
		rc = sqlite3_exec(db, "rollback;", NULL, 0, &zErrMsg);
		checkErr(rc, zErrMsg);
		if (canBuy) cout << "account id " << i << " can buy the car" << endl;
	}
	delete(accounts);
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc;
	int* iM = new int;
	int* oM = new int;
	stringstream ss;
	stringstream ss2;
	stringstream ss3;
	ss << from;
	ss2 << to;
	ss3 << amount;
	string i = ss.str();
	string o = ss2.str();
	string money = ss3.str();
	string iQuery = "select balance from accounts where id=" + i + ";";
	string oQuery = "select balance from accounts where id=" + o + ";";

	rc = sqlite3_exec(db, iQuery.c_str(), callback, iM, &zErrMsg);
	checkErr(rc, zErrMsg);
	rc = sqlite3_exec(db, oQuery.c_str(), callback, oM, &zErrMsg);
	checkErr(rc, zErrMsg);

	ss3.str(std::string());
	ss3 << (*oM + amount);
	string plus = ss3.str();
	ss3.str(std::string());
	ss3 << (*iM - amount);
	string minus = ss3.str();
	
	rc = sqlite3_exec(db, "begin transaction;", NULL, 0, &zErrMsg);
	checkErr(rc, zErrMsg);
	string oUpdate = "update accounts set balance=" + plus + " where id=" + o + ";";
	string iUpdate = "update accounts set balance=" + minus + " where id=" + i + ";";

	rc = sqlite3_exec(db, iUpdate.c_str(), NULL, 0, &zErrMsg);
	checkErr(rc, zErrMsg);
	rc = sqlite3_exec(db, oUpdate.c_str(), NULL, 0, &zErrMsg);
	checkErr(rc, zErrMsg);
	rc = sqlite3_exec(db, iQuery.c_str(), callback, iM, &zErrMsg);
	checkErr(rc, zErrMsg);

	if (*iM < 0) {
		rc = sqlite3_exec(db, "rollback;", NULL, 0, &zErrMsg);
		checkErr(rc, zErrMsg);
		delete iM;
		delete oM;
		return false;
	}
	else{
		rc = sqlite3_exec(db, "commit;", NULL, 0, &zErrMsg);
		checkErr(rc, zErrMsg);
	}
	delete iM;
	delete oM;
	return true;
}


bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc;
	int* money = new int;
	int* needed = new int;
	int* available = new int;
	stringstream ss;
	ss << buyerid;
	string buyer = ss.str();
	stringstream ss2;
	ss2 << carid;
	string car = ss2.str();

	string query = "select balance from accounts where buyer_id=" + buyer + ";"; 
	string query2 = "select price from cars where id=" + car + ";"; 
	string checkExist = "select available from cars where id=" + ss2.str() + ";"; 

	rc = sqlite3_exec(db, query.c_str(), callback, money, &zErrMsg); checkErr(rc, zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		exit(1);
	}
	rc = sqlite3_exec(db, query2.c_str(), callback, needed, &zErrMsg); checkErr(rc, zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		exit(1);
	}
	rc = sqlite3_exec(db, checkExist.c_str(), callback, available, &zErrMsg); checkErr(rc, zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		exit(1);
	}

	if (*available == 2 || *available == 0 || *money < *needed) return false;
//	std::cout << "After purchase You have: " << (*money - *needed) << " money in your pocket" << endl;
	ss.str(std::string());
	ss << (*money - *needed);

	string transfer = "update accounts set balance=" + ss.str() + " where id=" + buyer + ";";
	string updateCars = "update cars set available=0 where id=" + car + ";";

	rc = sqlite3_exec(db, transfer.c_str(), NULL, 0, &zErrMsg);
	checkErr(rc, zErrMsg);
	rc = sqlite3_exec(db, updateCars.c_str(), NULL, 0, &zErrMsg);
	checkErr(rc, zErrMsg);

	delete(money);
	delete(needed);
	delete(available);
	return true;
}

void checkErr(int rc, char* zErrMsg)
{
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		exit(1);
	}
}

void checkOpen(sqlite3* db, int rc)
{
	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		exit(1);
	}
}
