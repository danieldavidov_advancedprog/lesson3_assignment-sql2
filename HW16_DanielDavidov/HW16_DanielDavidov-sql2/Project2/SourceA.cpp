#include "sqlite3.h"
#include <iostream>
using namespace std;

int callback(void* idc, int argc, char* argv[], char* idcagain[])
{
	for (int i = 0; i < argc; i++) cout << argv[i] << " ";
	return 0;
}

int main()
{
	char err;
	int rc;
	bool flag = true;
	char *zErrMsg = 0;
	sqlite3* db;
	
	rc = sqlite3_open("firstPart.db",&db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	rc = sqlite3_exec(db, "CREATE TABLE people(id INTEGER PRIMARY KEY AUTOINCREMENT, name STRING);", NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	rc = sqlite3_exec(db, "INSERT INTO people(name) values('Ronen');", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "INSERT INTO people(name) values('Daniel');", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "INSERT INTO people(name) values('Shloma');", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "select * from people", callback,0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	sqlite3_close(db);

	system("PAUSE");
	return 0;
}