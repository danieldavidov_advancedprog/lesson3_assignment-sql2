#include "Client.h"

CryptoDevice cryptoDevice;


void Client::writePort(){
	try{
		ofstream out;
		out.open("ports.txt");
		int port = getPort(_myClient.socket);
		string toWrite = to_string(_myClient.id) + " " + to_string(port);
		out.write(toWrite.c_str(), toWrite.length());
		out.close();
	}
	catch (exception& e){ cout << e.what(); };
};


int Client::getPort(SOCKET client){

	struct sockaddr_in sin;
	int addrlen = sizeof(sin);
	if (getsockname(client, (struct sockaddr *)&sin, &addrlen) == 0 &&
		sin.sin_family == AF_INET &&
		addrlen == sizeof(sin))
	{
		int local_port = ntohs(sin.sin_port);
		cout << "The local port is " << local_port << endl;
		return local_port;
	}
	else throw invalid_argument("Error in socket"); // handle error;
};

void Client::initialize(struct addrinfo& hints)
{

	cout << "Starting Client...\n";
	WSADATA wsa_data; int iResult = 0;
	iResult = WSAStartup(MAKEWORD(2, 2), &wsa_data);
	if (iResult != 0) {
		cout << "WSAStartup() failed with error: " << iResult << endl;
		exit(1);
	}
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
}

int Client::connectToServer(struct addrinfo& hints, bool md5, string name)
{
	struct addrinfo *result = NULL, *ptr = NULL;
	cout << "Connecting...\n";
	if (md5 && cryptoDevice.authenticate(name) == false)
	{
		perror("permission denied\n");
		system("PAUSE");
		exit(1);
	}
	// Resolve the server address and port
	int iResult = getaddrinfo(IP_ADDRESS, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		cout << "getaddrinfo() failed with error: " << iResult << endl;
		WSACleanup();
		system("pause");
		return 1;
	}
	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
		// Create a SOCKET for connecting to server
		_myClient.socket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (_myClient.socket == INVALID_SOCKET) {
			cout << "socket() failed with error: " << WSAGetLastError() << endl;
			WSACleanup();
			system("pause");
			return 1;
		}
		// Connect to server.
		iResult = connect(_myClient.socket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(_myClient.socket);
			_myClient.socket = INVALID_SOCKET;
			continue;
		}
		break;
	}
	freeaddrinfo(result);
	if (_myClient.socket == INVALID_SOCKET) {
		cout << "Unable to connect to server!" << endl;
		WSACleanup();
		system("pause");
		return 1;
	}
	cout << "Successfully Connected" << endl;
}

int Client::obtainID()
{
	//Obtain id from server for this client;
	recv(_myClient.socket, _myClient.received_message, DEFAULT_BUFLEN, 0);
	string message = _myClient.received_message;
	if (message == "Server is full"){
		cout << message;
		return -1;
	}
	_myClient.id = atoi(_myClient.received_message);
	cout << "recieved the id" << endl;
	writePort();
	return 0;
}

int Client::process_client()
{
	while (1)
	{
		memset(_myClient.received_message, 0, DEFAULT_BUFLEN);

		if (_myClient.socket != 0)
		{
			int iResult = recv(_myClient.socket, _myClient.received_message, DEFAULT_BUFLEN, 0);
			string strMessage(_myClient.received_message);
			
			// some logic, we dont want to decrypt notifications sent by the operator
			// our protocol says - ": " means notification from the operator
			if (strMessage == "ok") continue;

			size_t position = strMessage.find(": ") + 2;
			string prefix = strMessage.substr(0, position);
			string postfix = strMessage.substr(position);
			string decrypted_message;

			//this is the only notification we use right now :(
			if (postfix != "Disconnected")
				//please decrypt this part!
				decrypted_message = cryptoDevice.decryptAES(postfix);
			else
				//dont decrypt this - not classified and has not been ecrypted! 
				//trying to do so may cause errors
				decrypted_message = postfix;

			if (iResult != SOCKET_ERROR)
				cout << prefix + decrypted_message << endl;
			else
			{
				cout << "recv() failed: " << WSAGetLastError() << endl;
				break;
			}
		}
	}
	writePort();
	cout << "got there\n";
	if (WSAGetLastError() == WSAECONNRESET)
		cout << "The server has disconnected" << endl;

	return 0;
}
/* Note:: can't connect to port used in the connection client -> server
 Send message to server send the public encryption only if the id matches (back to server)
 server sends key to client that sends cypher to server that sends everyone
 that try to decrypt it using their private key and display it
 Functionality:
Client:	Sending "Get me public key of id 3" to Server
Server: Sends it to id 3 // to everyone: someone can send his encryption before id 3
Client:	if I get that kind of message I send the key (the id is me)
Server: sends it to asker // to everyone: someone can say he was the sender id (he's faker)
The reciever encrypts, sends to server
Server: sends to everyone 
Client id 3: decrypts it with private key
*/
/*
string Client::askPeerToEncryptByID(int id){
	
	string send = "get me your public";
	wprintf(L"Sending a datagram to the receiver...\n");
	int iResult = sendto(_sendTo.socket,
		send.c_str() , send.length(), 0, (SOCKADDR *)& RecvAddr, sizeof(RecvAddr));
	if (iResult == SOCKET_ERROR) {
		wprintf(L"sendto failed with error: %d\n", WSAGetLastError());
		closesocket(_sendTo.socket);
		WSACleanup();
		exit(1);
	}
};*/

void Client::shutDown()
{
	cout << "Shutting down socket..." << endl;
	int iResult = shutdown(_myClient.socket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		cout << "shutdown() failed with error: " << WSAGetLastError() << endl;
		closesocket(_myClient.socket);
		WSACleanup();
		system("pause");
		exit(1);
	}

	closesocket(_myClient.socket);
	WSACleanup();
}

bool Client::sendMsg(string cipher){
	int len = cipher.length();
	int iResult = send(_myClient.socket, cipher.c_str(), len, 0);
	if (iResult <= 0)
	{
		cout << "send() failed: " << WSAGetLastError() << endl;
		return false;
	}
	return true;

};


Client::Client()
{

	struct addrinfo hints;
	_myClient = { INVALID_SOCKET, -1, "" };
	initialize(hints);
	connectToServer(hints, false, "Daniel");
	if (Client::obtainID() == -1) throw(invalid_argument("error in connection\n"));

}


Client::~Client(){}

bool Client::recievedMessage()
{
	char buf[3] = {0};
	int iResult = recv(_myClient.socket, buf, 3, 0);
	if (("ok" == buf))  return true;
	return false;
}

int main()
{
	Client cl;
	string sendTo;
	string sent_message = "get Me your public";
	thread my_thread(&Client::process_client,cl);
	
	while (1)
	{
		getline(cin, sent_message);
		if (!cl.sendMsg(sent_message)) break;
	}

		//Shutdown the connection since no more data will be sent
	my_thread.detach();

	cl.shutDown();
	system("pause");
	return 0;
}

/*
//if (justSentMsg) if (cl.recievedMessage()) justSentMsg = false; // recieve !
if (!justSentOk){ // Please let me know I can talk to you
cout << "Now type your message" << endl;
fflush(stdin);
getline(cin, sent_message);
if (!cl.sendMessage(sent_message)) break; // send !
else justSentOk = true;
cout << "sent out msg !!" << endl;
}
if (justSentOk )
{
if (!justRecievedOk){
if (cl.recievedMessage()) justRecievedOk = true; // recieve !

}
if (justRecievedOk) // Server accepts, he can communicate
{

//top secret! please encrypt
//string cipher = cryptoDevice.encryptAES(sent_message);
if (!cl.sendMessage("ok")) break;
cout << "just sent ok" << endl;
justSentOk = false;
justRecievedOk = false;
//justSentMsg = true;
}

}


*/