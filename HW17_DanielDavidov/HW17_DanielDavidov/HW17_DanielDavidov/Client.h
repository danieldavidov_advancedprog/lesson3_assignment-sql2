#pragma once

#include "CryptoDevice.h"
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include <string>
#include <thread>
#include <fstream>

#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_BUFLEN 512            
#define IP_ADDRESS "127.0.0.1"
#define DEFAULT_PORT "8202"
struct client_type
{
	SOCKET socket;
	int id;
	char received_message[DEFAULT_BUFLEN];
};

class Client
{
public:
	Client();
	~Client();

	bool connectByPort(int port);
	int getPortByIdFromServer(int id);
	string askPeerToEncryptByID(int id);
	void writePort();
	void shutDown();
	int process_client();
	bool sendMsg( string cipher);
	bool recievedMessage();

	int main();

private:
	

	void initialize(struct addrinfo& hints);
	int connectToServer(struct addrinfo& hints,bool aes, string name);
	int obtainID();
	int getPort(SOCKET client);
	

	string priv;
	string pub;
	client_type _myClient;
	client_type _sendTo;
};

