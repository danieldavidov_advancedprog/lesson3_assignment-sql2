#pragma once
#pragma comment(lib, "..\\cryptopp565\\Win32\\Output\\Debug\\cryptlib.lib")

#define TIMES 3

#include <cstdio>
#include <iostream>
#include "..\\cryptopp565\\osrng.h"
//#include "cryptopp565\\osrng.h"
#include "..\\cryptopp565\\modes.h"
#include "..\\cryptopp565\\md5.h"
#include "..\\cryptopp565\\hex.h"
#include "..\\cryptopp565\\rsa.h"
#include "..\\cryptopp565\\integer.h"
#include <string>
#include <cstdlib>

using namespace std;
using CryptoPP::Integer;
using CryptoPP::RSA;
using CryptoPP::AutoSeededRandomPool;

class CryptoDevice
{

public:
	CryptoDevice();
	~CryptoDevice();
	string encryptAES(string);
	string decryptAES(string);
	string md5(string);
	string encryptRsa(string);
	string decryptRsa();
	bool authenticate(string);
};
