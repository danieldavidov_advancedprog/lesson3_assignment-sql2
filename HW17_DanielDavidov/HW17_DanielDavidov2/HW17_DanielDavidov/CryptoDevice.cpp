#include "CryptoDevice.h"

//aquire a default key for AES and block size for CBC. can't generate randomly, since all the agents shuold have the same key.
//can initialize with a unique sequence but not needed.
byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];
byte digest[CryptoPP::MD5::DIGESTSIZE];

RSA::PrivateKey privKey;
RSA::PublicKey pubKey;
Integer c;

CryptoDevice::CryptoDevice(){}
CryptoDevice::~CryptoDevice(){}

string CryptoDevice::encryptAES(string plainText)
{

	string cipherText;

	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(cipherText));
	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(plainText.c_str()), plainText.length() + 1);
	stfEncryptor.MessageEnd();

	return cipherText;
}

string CryptoDevice::decryptAES(string cipherText)
{
	string decryptedText;

	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedText));
	stfDecryptor.Put(reinterpret_cast<const unsigned char*>(cipherText.c_str()), cipherText.size());
	stfDecryptor.MessageEnd();

	return decryptedText;
}

string CryptoDevice::encryptRsa(string message)
{
	// Below, the values for d and e were swapped
	Integer n("0xbeaadb3d839f3b5f"), e("0x11"), d("0x21a5ae37b9959db9");
	privKey.Initialize(n, d, e);
	pubKey.Initialize(n, d);
	Integer m;
	// Treat the message as a big endian array
	m = Integer((const byte *)message.data(), message.size());
	// Encrypt
	c = pubKey.ApplyFunction(m);
	cout << "c: " << hex << c << endl;
	
	stringstream ss;
	ss << std::hex << c;
	return ss.str();
}

string CryptoDevice::decryptRsa()
{
	string recovered;
	Integer r;
	AutoSeededRandomPool prng;
	r = privKey.CalculateInverse(prng, c);
	cout << "r: " << hex << r << endl;

	// Round trip the message
	size_t req = r.MinEncodedSize();
	recovered.resize(req);
	r.Encode((byte *)recovered.data(), recovered.size());
	cout << "recovered: " << recovered << endl;

	stringstream ss;
	ss << hex << r;
	return ss.str();
}

string CryptoDevice::md5(string message)
{
	CryptoPP::MD5 hash;
	hash.CalculateDigest(digest, (const byte*)message.c_str(), message.length());

	CryptoPP::HexEncoder encoder;
	std::string output;

	encoder.Attach(new CryptoPP::StringSink(output));
	encoder.Put(digest, sizeof(digest));
	encoder.MessageEnd();

	return output;
}

bool CryptoDevice::authenticate(string name)
{
	string md = md5(name);
	string i;
	string pass;
	cout << name << " moneyMaker" << endl;
	cout << "{MUSTNOTPRESENTONLYFORDEBUGGING!!!" <<  md << "}" << endl;    
	for (int k = 0; k < TIMES; k++)
	{
		cout << "name: ";
		cin >> i;
		cout << endl;
		if (name.compare(i) == 0)
		{
			cout << "password: ";
			cin >> pass;
			cout << endl;
			if (md.compare(pass) == 0) return true;
		}
		
	}
	return false;
}