#include "Read_file.h"


Read_file::Read_file()
{
}

//u can do it by your self, but this is not the purpose of the exercise so here you go


string Read_file::read(string file_name){
	string text, line;
	ofstream myfile;
	ifstream encrypted(file_name);

	if (encrypted.is_open()){
		while (getline(encrypted, line)){
			text += line + '\n';
		}
		encrypted.close();
	}
	return text;

}

Read_file::~Read_file()
{
}
