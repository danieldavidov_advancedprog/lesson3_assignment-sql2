#pragma once
#pragma comment(lib, "cryptopp565\\Win32\\Output\\Debug\\cryptlib.lib")

#include <cstdio>
#include <iostream>
#include "cryptopp565\osrng.h"
#include "cryptopp565\des.h"
#include "cryptopp565\modes.h"
#include <string.h>
#include <cstdlib>

using namespace std;


class CryptoDevice
{

public:
	CryptoDevice();
	~CryptoDevice();
	static string encryptAES(string);
	static string decryptAES(string);

};
