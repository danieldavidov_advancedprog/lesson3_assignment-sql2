#include <iostream>
#include <Windows.h>
#include "Helper.h"
#include <tchar.h>

#pragma warning(disable: 4996)

typedef int(__cdecl *MYPROC)(LPWSTR);
#define PATH_MAX 1024

using namespace std;

void cd(TCHAR* dir)
{
	if (!SetCurrentDirectory(dir))
	printf("SetCurrentDirectory failed (%d)\n", GetLastError());
}

void createFile(TCHAR* name)
{
	HANDLE hFile = CreateFile(name,                // name of the write
		GENERIC_WRITE,          // open for writing
		0,                      // do not share
		NULL,                   // default security
		CREATE_NEW,             // create new file only
		FILE_ATTRIBUTE_NORMAL,  // normal file
		NULL);                  // no attr. template
	if (hFile == INVALID_HANDLE_VALUE)
		cout << (TEXT("Terminal failure: Unable to open file \"%s\" for write with error:(%d).\n"), name,GetLastError()) << endl;
}

void printDir(TCHAR* directory,TCHAR* all)
{
	LARGE_INTEGER filesize;
	HANDLE dir;
	WIN32_FIND_DATA file_data;
	_tcscat(all, "/*");
	if ((dir = FindFirstFile((TCHAR*)all, &file_data)) == INVALID_HANDLE_VALUE)
	{
		cout << "no files found error: " << GetLastError() << endl;
		return; /* No files found */
	}
	do {
		/*const string file_name = file_data.cFileName;
		const bool is_directory = (file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;
		if (file_name[0] == '.')
			continue;
		if (is_directory)
			cout << "<DIR>\t";
		else cout << "\t";
		cout << file_name << endl;*/
		if (file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			_tprintf(TEXT("  %s   <DIR>\n"), file_data.cFileName);
		}
		else
		{
			filesize.LowPart = file_data.nFileSizeLow;
			filesize.HighPart = file_data.nFileSizeHigh;
			_tprintf(TEXT("  %s   %ld bytes\n"), file_data.cFileName, filesize.QuadPart);
		}
	} while (FindNextFile(dir, &file_data));

	FindClose(dir);
}


void loadFunc(TCHAR* dll)
{
	HINSTANCE hinstLib;
	MYPROC ProcAdd;
	BOOL fFreeResult, fRunTimeLinkSuccess = FALSE;

	// Get a handle to the DLL module.

	hinstLib = LoadLibrary(TEXT(dll));

	// If the handle is valid, try to get the function address.
	
	if (hinstLib != NULL)
	{
		ProcAdd = (MYPROC)GetProcAddress(hinstLib, "TheAnswerToLifeTheUniverseAndEverything"); // TheAnswerToLifeTheUniverseAndEverything

		// If the function address is valid, call the function.

		if (NULL != ProcAdd)
		{
			fRunTimeLinkSuccess = TRUE;
			cout << (ProcAdd)(L"Message sent to the DLL function\n") << endl;
		}
		// Free the DLL module.

		fFreeResult = FreeLibrary(hinstLib);
	}

	// If unable to call the DLL function, use an alternative.
	if (!fRunTimeLinkSuccess)
		printf("Message printed from executable\n");

}

void execute(TCHAR* exe)
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	DWORD exitCode;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	// Start the child process. 
	if (!CreateProcess(NULL,   // No module name (use command line)
		exe,        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi)           // Pointer to PROCESS_INFORMATION structure
		)
	{
		printf("CreateProcess failed (%d).\n", GetLastError());
		return;
	}



	// Wait until child process exits.
	WaitForSingleObject(pi.hProcess, INFINITE);

	if (GetExitCodeProcess(pi.hProcess, &exitCode))
	{
		//Check the code here
		if (exitCode == STATUS_PENDING)
			cout << ("still alive");
		else
			printf("code = %X\n", exitCode);
	}
	else
		cout << ("Failed to get exit code") << endl;

	// Close process and thread handles. 
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
}

void doSome(vector<string> buff,TCHAR* dir )
{
	string abc = buff[0];
	if (abc == "pwd") cout << dir << endl;
	if (abc == "cd"){ 
		if (PATH_MAX < buff[1].size()) { cout << ("error in size of path"); }
		cd((TCHAR*)(buff[1].c_str())); 
	}
	else if (abc == "create") createFile((TCHAR*)(buff[1].c_str()));
	else if (abc == "ls") printDir(dir,dir);
	else if (abc == "Secret") loadFunc((TCHAR*)buff[0].c_str());
	else if (abc.find(".exe") != string::npos) execute((TCHAR*)abc.c_str());
}



int main()
{
	
	string curr;
	vector<string> get;
	cout << "enter quit to quit" << endl;
	while (true)
	{
		TCHAR currentDir[MAX_PATH];
		DWORD dwRet = GetCurrentDirectory(MAX_PATH, currentDir);
		if (dwRet == 0) { printf("GetCurrentDirectory failed (%d)\n", GetLastError()); exit(1); }
		cout << ">>";

		getline(std::cin,curr);
		if (curr == "quit") break;
		get = Helper::get_words(curr);
		for (int i = 0; i < get.size(); i++) Helper::trim(get[i]);

		doSome(get,currentDir);
	}
	return 0;
}
