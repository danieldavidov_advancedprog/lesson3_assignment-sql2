.open magshimim.db
create table StudentsInClasses(studentID int,classID int);
create table Teachers(teacherID int,teacherName text);
create table Categories(catID int, catName text);
create table Classes(classID int,className text,teacherID int, catID int);
create table Students (studentID int,studentName text,studentAge int,studentGrade int);